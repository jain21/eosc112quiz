# **UBC EOSC 112 Quiz** #
###Interactive Quiz built using Node.js, Mongoose (for MongoDB) and Knockout.js

**Node Version: ** 0.10.28  
**Start App: ** node lib/server.js  

### WorkFlow
* Make sure mongo is connected on port 27017 (default)
* ````grunt watch```` in the directory to auto-compile less to css
* start the server, ````node lib/server.js````
* open the quiz on ````www.localhost:8080.com````

###Directory Structure              
* **lib/**
> contains the express server initialization, imports other modules from the directory, initialize the DB & its schema on server-start, initialize the Sockets. 

* **middleware/**
> Route to the correct path based on incoming requests

* **config/**
> ````config.json```` a center-location for a config info for server & db atm; 

* **app/**
>  **Controller** : View controller for incoming requests. RPC call to invoke methods for ````Question```` collection.  
**Model** : Schema & Initialization of ````Question```` Collection.

* **public/**
> Consists of client-side JS, assets, css.

* **views/**
> HTML content for different pages (views)

****Some Notes:****  
* ````public/js/main.js```` : Config of RequireJS  
* ````public/js/rpc.js```` : Client-side socket initialization to invoke server-side RPC calls.  
* ````public/js/start.js```` : Knockout Model Definition for the Quiz.  

#### Feel free to give Pull Requests for any bug fixes, features, better ways of doing and shipping things. Feel free to get in touch on ````jainshreyans@outlook.com````

### License
No restrictions on using the code. :)