'use strict';

/*
 * GruntFile for
 * Autocompiling of Less to CSS
*/

module.exports = function(grunt) {
    grunt.initConfig({
        less: {
            development: {
                options: {
                    compress: true,
                    yuicompress: true,
                    optimization: 2
                },
            files: [{
                 expand: true,
                 cwd: './public/less',
                 src: '*.less',
                 dest: './public/css',
                 ext: '.css'
            }]
          }
        },
        watch: {
            styles: {
                files: ['./public/less/*.less'],
                tasks: ['less'],
                options: {
                    nospawn: true
                }
            }
        }
    });

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['watch']);

};