// This is the main requirejs script

requirejs.config({
    // This defines aliases for requirejs modules, so you don't have to type the full path:
    paths: {
        'knockout' : 'knockout',
        'rpc' : 'rpc',
        'jquery' : 'jquery'
    },
    // Some non-requirejs libraries need their dependencies configured for them:
    shim: {
        'jquery.bootstrap': { deps: ['jquery'] }
    }
});