// Here's my data model

require(['knockout','rpc', 'jquery'], function(ko, rpc, $) {

'use strict';

    function QuizIntroViewModel(data) {
        var self = this;

        self.topics = ko.observableArray(data.topics);
        self.type = ko.observableArray(data.type);
        self.selectedSubTopics = ko.observableArray();
        self.selectedType = ko.observableArray();
        self.introScreen = ko.observable(true);

        self.inputModel = ko.observable();
        self.image = ko.observable('');

        self.selectedOption = ko.observable();

        self.allQuestions = ko.observableArray([]);
        self.nextQuestionIndex = ko.observable(0);

        self.showCheckButton = ko.observable(false);
        self.prependImageURL = ko.observable('');
        self.error = ko.observable('');
        self.mailToHref = ko.observable('');
        self.timer = ko.observable('00:00:00');

        self.showNextButton = ko.computed(function(){
            return (self.nextQuestionIndex() < self.allQuestions().length) ? true : false;
        });

        self.startQuiz = function startQuiz(){
            var data = {
                'class' : 'eosc112',
                'topic' : self.selectedSubTopics(),
                'type'  : self.selectedType()
            };

            if(!self.selectedSubTopics().length || !self.selectedType().length){
                self.error('Please choose both the options!');
                return;
            }

            rpc.call('question.getByTopicAndType', data, function(err, res){
                if(err){
                    return;
                }

                self.allQuestions(res);
                if(res && res.length){
                    self.inputModel(new QuestionViewModel(res[0]));
                    self.nextQuestionIndex(1);
                    self.introScreen(false);
                    self.prependImageURL(self.inputModel().topic[0]);
                    if(self.inputModel().image !== '')
                        self.image(self.prependImageURL() + '/' + self.inputModel().image + '.png');
                    else
                        self.image('');

                    self.mailToHref('mailto:jainshreyans@outlook.com?subject=[EOSC112] Report Incorrect Result for ID ' + self.inputModel().id);
                    clearTimer();
                    startTimer();
                }
                else {
                    self.error('Questions Not Found..');
                }
            });
        };

        self.answerSelected = function answerSelected(id){
            $('.frame-option').find('.question-text-option').removeClass('answer-incorrect');
            $('.frame-option').find('.question-text-option').removeClass('answer-correct');
            $('.frame-option').find('.question-text-option').removeClass('answer-selected');
            $('#'+id).find('.question-text-option').addClass('answer-selected');
            self.selectedOption(id);
            self.showCheckButton(true);
        };

        self.checkForAnswer = function () {
            if(self.selectedOption() === self.inputModel().answer){
                $('.frame-option').find('.question-text-option').removeClass('answer-selected');
                $('#'+self.selectedOption()).find('.question-text-option').addClass('answer-correct');
            } else {
                $('.frame-option').find('.question-text-option').removeClass('answer-selected');
                $('#'+self.selectedOption()).find('.question-text-option').addClass('answer-incorrect');
                $('#'+self.inputModel().answer).find('.question-text-option').addClass('answer-correct');
            }

            self.showCheckButton(false);
            stopTimer();
        };

        self.nextQuestion = function() {
            var allQuestionsLength = self.allQuestions().length;
            if(self.nextQuestionIndex() < allQuestionsLength){
                self.inputModel(new QuestionViewModel(self.allQuestions()[self.nextQuestionIndex()]));
                self.nextQuestionIndex(self.nextQuestionIndex()+1);
                self.prependImageURL(self.inputModel().topic[0]);
                if(self.inputModel().image !== '')
                    self.image(self.prependImageURL() + '/' + self.inputModel().image + '.png');
                else
                    self.image('');

                self.mailToHref('mailto:jainshreyans@outlook.com?subject=[EOSC112] Report Incorrect Result for ID ' + self.inputModel().id);
                stopTimer();
                clearTimer();
                startTimer();
            }
        };

        // StopWatch
        var timer, seconds, minutes, hours;

        function startTimer() {
            timer = setTimeout(addTimeToTimer, 1000);
        }

        function addTimeToTimer() {
            seconds++;
            if (seconds >= 60) {
                seconds = 0;
                minutes++;
                if (minutes >= 60) {
                    minutes = 0;
                    hours++;
                }
            }
            var time = (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds);

            self.timer(time);
            startTimer();
        }

        function stopTimer(){
            clearTimeout(timer);
        }

        function clearTimer(){
            seconds = 0;
            minutes = 0;
            hours = 0;
            self.timer('00:00:00');
        }

    }

    function QuestionViewModel(data) {
        var data = data || {};
        return {
            id : data._id,
            type : data.type,
            topic : data.topic,
            question : data.question,
            option : data.option,
            answer : data.answer,
            image : data.image
        };
    }

    var dataRes = {
        'class' : 'eosc112'
    };

    rpc.call('question.getDistincTopicType', dataRes, function(err, res){
        if(err){
            return;
        }

        var topicTitles = res.topic.map(function(topic){
            var data = {
                id : topic.remove(/[0-9]/g),
                topic : topic.remove(/[0-9]/g).dasherize().capitalize(true),
                showSubTopics : ko.observable(false),
                subTopic : []
            };

            return data;
        }).unique('id');

        res.topic.map(function(topic){
            var subTopic = {
                id : topic,
                label : topic.dasherize().capitalize(true)
            };

            var mainTopic = topicTitles.find({
                id : topic.remove(/[0-9]/g)
            });

            if(mainTopic){
                mainTopic.subTopic.push(subTopic);
            }
        });

        var quizType = res.type.map(function(type){
            return {
                id : type,
                label : type.dasherize().capitalize(true)
            }
        });

        var quizDataModelData = {
            topics : topicTitles,
            type : quizType
        };

        // sort by A-Z
        quizDataModelData.topics = quizDataModelData.topics.sortBy(function(topic){
            return topic.id;
        });

        // Apply the KO bindings
        ko.applyBindings(new QuizIntroViewModel(quizDataModelData));
    });

});