define(function(){
    var socket = io.connect('http://localhost:8080');

    socket.on('test', function(data) {
        console.log('new TEst data is '+data);
    });

    socket.on('disconnect', function () {
       console.log('disconnect client event....');
    });

    window.onbeforeunload = function(e) {
      socket.disconnect();
    };

    function rpcCall(call, args, cb){
        socket.emit('modelData',call, args, function(err, data){
            cb(null, data);
        });
    }

    return {
        call : rpcCall
    };

});