'use strict';

/**
* Question Model Controller
*/

var mongoose = require('mongoose');
var Question = mongoose.model('Question');
var async = require('async');
var logger = require('../../../lib/logger');

module.exports = {
    getAll : getAll,
    getByAllByType : getByAllByType,
    getByTopic : getByTopic,
    getByTopicAndType : getByTopicAndType,
    getDistincTopicType : getDistincTopicType
};

// Returns everything
function getAll (args, cb) {
    var query;

    query = Question.find({});
    query.select('question option image topic section');
    query.exec(function(err, docs) {
        if(err){
            logger.error('getAll error is '+JSON.stringify(err));
            cb(err);
            return;
        }
        cb(null, docs.randomize())
    });
}

// Returns All Types
function getByAllByType (data, cb) {
    var query;

    query = Question.find({ 'class' : data['class'] });
    query.select('question option image topic answer type');
    query.where('type').equals(data.type);
    query.exec(function(err, docs) {
        if(err){
            logger.error('getByAllByType error is '+JSON.stringify(err));
            cb(err);
            return;
        }
        cb(null, docs.randomize())
    });
}

// Returns By Topic
function getByTopic (data, cb) {
    var query;

    query = Question.find({ 'class' : data['class'] });
    query.select('question option image topic answer type');
    query.where('topic').in(data.topic);
    query.exec(function(err, docs) {
        if(err){
            logger.error('getByAllByType error is '+JSON.stringify(err));
            cb(err);
            return;
        }
        cb(null, docs.randomize())
    });
}

// Returns By Topic and Type
function getByTopicAndType (data, cb) {
    var query;

    query = Question.find({ 'class' : data['class'] });
    query.select('question option image topic answer type');
    query.where('topic').in(data.topic);
    query.where('type').in(data.type);
    query.exec(function(err, docs) {
        if(err){
            logger.error('getByTopicAndType error is '+JSON.stringify(err));
            cb(err);
            return;
        }
        cb(null, docs.randomize());
    });
}

// Get Distinct Topic AND Type
function getDistincTopicType (data, cb) {
    async.parallel({
        topic: function (callback){
            Question.distinct('topic', { 'class' : data.class }, function(err, res) {
                if(err){
                    callback(err);
                    return;
                }
                callback(null, res);
            });
        },
        type: function (callback){
            Question.distinct('type', { 'class' : data.class }, function(err, res) {
                if(err){
                    callback(err);
                    return;
                }
                callback(null, res);
            });
        }
    }, function(err, result){
        if(err){
            cb(err);
            return;
        }
        cb(null, result);
    });
}