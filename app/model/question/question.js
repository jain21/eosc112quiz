'use strict';

/**
* Question Model
*/

var mongoose = require('mongoose');
var logger = require('winston');

module.exports = function() {

    var mongooseSchema, schema, Question;

    mongooseSchema = mongoose.Schema;
    schema = require(__dirname + '/schema/question').schema;
    Question = new mongooseSchema(schema);

    mongoose.model('Question', Question);

    if(process.env.NODE_ENV !== 'test'){
        logger.info('Successfully Initialised the Question Schema');
    }
};