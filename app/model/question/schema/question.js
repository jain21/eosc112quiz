'use strict';

/**
* Question Schema Document
*/

/** Sample Data
* class : eosc112,
* topic : ['atmosphere1', 'atmosphere2',..],
* type : 'clicker' || 'studySection'
*/

module.exports.schema = {
    'class' : { type : String },
    'question' : { type: String, required: true },
    'option' : { type : Array, required: true },
    'image' : { type : String },
    'topic' : { type : Array },
    'type' : { type : Array },
    'answer' : { type : String },
    'meta' : {
        'createdOn' : { type: Date, default: new Date().toISOString() }
    }
}