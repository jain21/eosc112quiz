'use strict';

/**
* For Routing to the correct View Engine
*/

var logger = require('../lib/logger');
var url = require('url');

module.exports = function() {
    return function(req, res, next) {

        var path, pathname, viewPage;

        path = __dirname + '/../app/controller';
        pathname = url.parse(req.originalUrl).pathname

        if(pathname === '/') {
            viewPage = path + pathname + 'home';
        } else if(pathname !== '/favicon.ico') {
            viewPage = path + pathname;
        }

        try {
            // render the appropriate request
            if(viewPage)
                require(viewPage)(req, res);
        }
        catch(e) {
            // render 404 page
            //res.render('error');
            logger.error('Requesed Pathname: '+ pathname +' Not Found!');
        }
    }
}