'use strict';

/**
* Main Server
*/

var express = require('express')
var logger = require('./logger');
var errorhandler = require('errorhandler');
var config = require('./config');
var sugar = require('sugar');
var ejs = require('ejs');
var db = require('./db');
var io = require('socket.io');
var methodOverride = require('method-override');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var socketModule = require('./socket');
var viewMiddleware = require('../middleware/view.js');
require("amd-loader");

var app, router, validMethods, port,
    mongoose, sio;

app = express();
router = express.Router();
port = config.platform.port;

mongoose = db.mongoose();
sio = io.listen(app.listen(port));
socketModule.init(sio)

if (process.env.NODE_ENV === 'development') {
    app.use(errorhandler({
            dumpExceptions  : true,
            showStack       : true
        }));
}

app.engine('html', require('ejs').renderFile);

// using html (which is actually ejs) for views
app.set('views', __dirname + '/../views');
app.set('view engine', 'html');

// allow HTTP method override support
app.use(methodOverride());

// parse url-encoded form data or JSON
app.use(cookieParser());
app.use(bodyParser.json());
app.use(express.static(__dirname + '/../public'));

// For View HTML Engine
app.use('*', viewMiddleware());

logger.info('Webserver started, Listening on port '+port);

app.get('/abc', function(req, res){
  res.send('Hello World\n');
});