'use strict';

/**
* Sockets File
*/
var logger = require('./logger');
var sio;

module.exports = {
    init : init,
    emit : emit
}

function init(io) {
    sio = io;
    sio.sockets.on('connection', function(socket) {
        logger.info('socket ' + socket.id + ' connected!');

        socket.on('disconnect', function() {
            logger.info('socket ' + socket.id + ' disconnected!');
        });

        socket.on('modelData', function(rpcCall, args, cb){
            var model = rpcCall.split('.')[0];
            var method = rpcCall.split('.')[1];
            require('../app/controller/' + model + '/' + model)[method](args, function(err, data){
                if(err){
                    cb(err);
                    logger.error('Socket Error for model `'+ model + '` and method `' + method
                            + ' is '+ JSON.stringify(err));
                    return;
                }
                cb(null, data);
            });
        });
    });
}

function emit(alert, data) {
    logger.info('emitting `' + alert + '` with data '+JSON.stringify(data));
    sio.sockets.emit(alert, data);
}