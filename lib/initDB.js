/**
* Initializing All Version Model Schema
*/

var fs = require('fs');

var Models = {};

Models.loadAll = function() {

    var model_path = __dirname + '/../app/model';

    fs.readdirSync(model_path).forEach(function(dir){
        var path = model_path + '/' + dir;

        if(fs.lstatSync(path).isDirectory()){
            fs.readdirSync(path).forEach(function(d){
                var file = path + '/' + d;

                if(!fs.lstatSync(file).isDirectory()) {
                    if(~file.indexOf('.js'))
                        require(file)();
                }
            });
        }
    });
}

module.exports = Models;
