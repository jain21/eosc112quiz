/**
* Connecing to DB
*/

var mongoose = require('mongoose');
var initDB = require('./initDB');
var logger = require('winston');
var config = require('./config');

function make() {

    var connect = function connect() {
        mongoose.connect('mongodb://' + config.database.mongoDb.url);
    }
    connect();

    var db = mongoose.connection;
    db.on('error', function(){
         logger.error('MongoDB Connection Error:')
    });

    db.on('open', function(){
        logger.info('Successfully Connected to MongoDB');
        initDB.loadAll();
    });

    db.on('disconnected', function(){
        logger.info('MongoDB Disconnected. Trying to Reconnec Again ....');
        connect();
    });
}

module.exports.mongoose = make;